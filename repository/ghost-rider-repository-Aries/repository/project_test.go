package repository

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCreateMainProject(t *testing.T) {
	arg := CreateMainProjectParams{
		Code: "ET01",
		Name: "doraemon",
	}

	project, err := testQueries.CreateMainProject(context.Background(), arg)

	require.NoError(t, err)
	require.NotEmpty(t, project)
	require.Equal(t, arg.Code, project.Code)
	require.Equal(t, arg.Name, project.Name)
	require.NotZero(t, project.ID)
	require.NotZero(t, project.CreatedAt)

}
