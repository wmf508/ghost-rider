package repository

import (
	"os"
	"testing"

	_ "github.com/lib/pq"
	commonPg "gitlab.com/wmf508/alfred/database/postgres"
	commonLog "gitlab.com/wmf508/alfredy/log"
)

var testQueries *Queries
var log commonLog.Logger

func TestMain(m *testing.M) {
	log = commonLog.GetLogger()

	config := commonPg.Config{
		Username: "postgres",
		Password: "password",
		Address:  "localhost",
		Port:     5432,
		Database: "doraemon",
		SslMode:  "disable",
	}

	cli, err := commonPg.GetClient(config)
	if err != nil {
		log.Fatal("cannot connect to db", err)
	}

	testQueries = New(cli)

	os.Exit(m.Run())
}
