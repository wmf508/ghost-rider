package repository

import (
	"fmt"

	commonPg "gitlab.com/wmf508/alfred/database/postgres"
	"gitlab.com/wmf508/alfredy/fdp"
	commonLog "gitlab.com/wmf508/alfredy/log"

	// "gitlab.com/wmf508/ghost-rider-repository-Aries/repository"
	commonRepo "gitlab.com/wmf508/ghost-rider/repository"
)

type Repository struct {
	Fdp *fdp.Fdp
	Pg  *repository.Queries
}

// 汇集所有类型的repository到mainRepo中
func NewRepository(config commonPg.Config) (commonRepo.Repository, error) {

	log := commonLog.GetLogger()

	f := fdp.GetFdp()

	f.Log = log

	pg, err := commonPg.GetClient(config)
	if err != nil {
		return nil, fmt.Errorf("failed to create postgres client: %v", err)
	}

	queries := repository.New(pg)

	repo := &Repository{
		Pg:  queries,
		Fdp: f,
	}

	return repo, nil
}
