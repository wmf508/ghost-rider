// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.13.0
// source: project.sql

package repository

import (
	"context"
)

const createMainProject = `-- name: CreateMainProject :one
INSERT INTO main_projects (
	code,
	name
) VALUES (
	$1, $2
) RETURNING id, code, name, created_at
`

type CreateMainProjectParams struct {
	Code string `json:"code"`
	Name string `json:"name"`
}

func (q *Queries) CreateMainProject(ctx context.Context, arg CreateMainProjectParams) (MainProject, error) {
	row := q.db.QueryRowContext(ctx, createMainProject, arg.Code, arg.Name)
	var i MainProject
	err := row.Scan(
		&i.ID,
		&i.Code,
		&i.Name,
		&i.CreatedAt,
	)
	return i, err
}
