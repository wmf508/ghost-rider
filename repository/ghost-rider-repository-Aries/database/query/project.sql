-- name: CreateMainProject :one
INSERT INTO main_projects (
	code,
	name
) VALUES (
	$1, $2
) RETURNING *;