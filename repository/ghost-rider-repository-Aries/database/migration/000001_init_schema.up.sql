CREATE TABLE "main_projects" (
  "id" bigserial PRIMARY KEY,
  "code" varchar NOT NULL,
  "name" varchar NOT NULL,
  "created_at" timestamptz DEFAULT (now())
);
