dev:
	go run ./cmd/ghost-rider/main.go -dev	
test:
	@go test -cover ./...
package:
	go get -u gitlab.com/wmf508/alfred	
errorcode:
	go get -u gitlab.com/wmf508/alfred
.PHONY: test dev update errorcode createdb dropdb postgres sqlc