package handler

import (
	"fmt"

	"github.com/gin-gonic/gin"
	commonErrCode "gitlab.com/wmf508/alfred/errorcode"
	commonGin "gitlab.com/wmf508/alfred/rest/http/gin"
	"gitlab.com/wmf508/alfredy/fdp"
	"gitlab.com/wmf508/ghost-rider/service"
)

type Handler struct {
	Svc *service.Service
	*fdp.Fdp
}

// create project
func (hdl *Handler) CreateProject(c *gin.Context) {
	params := struct {
		ProjectCode           string `json:"project_code" binding:"required"`
		ErrorMapRepositoryUrl string `json:"errmap_repo_url" binding:"required"`
	}{}

	if err := c.BindJSON(&params); err != nil {

		desc, e := commonErrCode.GetDesc("E000020001")
		if e != nil {
			hdl.Log.Println(e.Error())
			commonGin.InvalidErrCodeResp(c, e.Error())
			return
		}

		hdl.Log.Printf("%v: %v", desc.GetLogMsgWithCode(), err.Error())
		// error 400
		commonGin.BadRequestErrorResp(c, desc.ErrorCode, fmt.Sprintf("%v:%v", desc.UserMsg, err.Error()))
		return
	}

	err := hdl.Svc.CreateProject(params.ProjectCode)
	if err != nil {
		// TODO
	}
	// TODO: complish create project logic.
}

func (hdl *Handler) CreateErrMap(c *gin.Context) {

	params := struct {
		ProjectCode string `json:"project_code" binding:"required"`
	}{}

	if err := c.BindJSON(&params); err != nil {

		desc, e := commonErrCode.GetDesc("E000020002")
		if e != nil {
			hdl.Log.Println(e.Error())
			commonGin.InvalidErrCodeResp(c, e.Error())
			return
		}

		hdl.Log.Printf("%v: %v", desc.GetLogMsgWithCode(), err.Error())
		// error 400
		commonGin.BadRequestErrorResp(c, desc.ErrorCode, fmt.Sprintf("%v:%v", desc.UserMsg, err.Error()))
		return
	}

	// err := hdl.Svc.CreateErrMap(projCode)
	// if err != nil {
	// 	// TODO:
	// }

	// TODO: complish create error map logic.
}
