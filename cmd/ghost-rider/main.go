package main

import (
	// "os"

	"gitlab.com/wmf508/alfredy/fdp"
	commonlog "gitlab.com/wmf508/alfredy/log"
	"gitlab.com/wmf508/ghost-rider/server"
	// "gopkg.in/src-d/go-git.v4"
)

func main() {
	// url := "https://gitlab.com/wmf508/alfredy.git"

	// _, err := git.PlainClone("./errcode-project", false, &git.CloneOptions{
	// 	URL:      url,
	// 	Progress: os.Stdout,
	// })

	// if err != nil {
	// 	log.Printf("failed to git clone project %s: %v", url, err)
	// }

	var log = commonlog.GetLogger()

	f := &fdp.Fdp{
		Log: log,
	}

	log.Println("server start")
	server.Run(f)
	log.Println("server shut down")
}
