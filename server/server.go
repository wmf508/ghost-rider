package server

import (
	"fmt"

	commonPg "gitlab.com/wmf508/alfred/database/postgres"
	"gitlab.com/wmf508/alfredy/fdp"
	"gitlab.com/wmf508/ghost-rider/handler"
	"gitlab.com/wmf508/ghost-rider/repository/ghost-rider-repository-Aries/repository"
	"gitlab.com/wmf508/ghost-rider/router"
	"gitlab.com/wmf508/ghost-rider/service"
)

type GitConfig struct {
	Url string
}

func NewService() (*service.Service, error) {
	// TODO: complish new service logic
	config := commonPg.Config{}

	repo, err := repository.NewRepository(config)
	if err != nil {
		return nil, fmt.Errorf("failed to create repository: %w", err)
	}

	svc := &service.Service{
		Repo: repo,
	}
	return svc, nil
}

// run server
func Run(fdp *fdp.Fdp) error {
	svc, err := NewService()
	if err != nil {
		return fmt.Errorf("failed to create service: %w", err)
	}
	svc.Fdp = fdp

	hdl := &handler.Handler{
		Svc: svc,
		Fdp: fdp,
	}
	ginEngine := router.InitApiRouters(hdl)

	// run server
	ginEngine.Run(":8080")
	return nil
}
