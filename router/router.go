package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/wmf508/ghost-rider/handler"
)

// set routers
func InitApiRouters(hdl *handler.Handler) *gin.Engine {
	r := gin.Default()

	// api v1
	apiV1Group := r.Group("/api/v1")

	// create errormap file in project
	apiV1Group.POST("/project", hdl.CreateProject)
	// create errormap file in project
	apiV1Group.POST("/errormap", hdl.CreateErrMap)

	return r
}
