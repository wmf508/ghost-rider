package service

import (
	"gitlab.com/wmf508/alfredy/fdp"
	"gitlab.com/wmf508/ghost-rider/repository"
)

type Service struct {
	Repo repository.Repository
	*fdp.Fdp
}

func (svc *Service) CreateProject(projCode string) error {
	err := svc.Repo.CreateMainProject()
	if err != nil {
		return err
	}

	return nil
}
